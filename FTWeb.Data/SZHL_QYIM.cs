//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace QJY.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SZHL_QYIM
    {
        public Nullable<int> ComId { get; set; }
        public int ID { get; set; }
        public string FromUserName { get; set; }
        public string MsgType { get; set; }
        public string Event { get; set; }
        public string ChatId { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string UserList { get; set; }
        public string AddUserList { get; set; }
        public string DelUserList { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Status { get; set; }
        public string Sourse { get; set; }
        public string Remark { get; set; }
    }
}
